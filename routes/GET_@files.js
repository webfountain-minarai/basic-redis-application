const reGet = require('../database/redis').getAsync;


async function getFiles(res, { name:fileName }) {
  console.log({ fileName });
  let fileMeta = await reGet(fileName);

  if (!fileMeta) fileMeta = `filename does not exist: ${fileName}`;

  res.writeHead(200, {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(fileMeta)
  });

  res.write(fileMeta);
  res.end();
}


module.exports = getFiles;
