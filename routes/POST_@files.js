const reSet = require('../database/redis').setAsync;


let configNum = 1;

async function postFiles(req, res) {
  let reqBody = [];

  req.on('data', chunk => reqBody.push(chunk));
  req.on('end', async () => {
    reqBody = Buffer.concat(reqBody).toString();

    const fileName = `config_${configNum}.json`;
    await reSet(fileName, reqBody);
    configNum++;

    const resBody = JSON.stringify({
      message: `filename: ${fileName}`
    });

    res.writeHead(201, {
      'Content-Length': Buffer.byteLength(resBody),
      'Content-Type': 'application/json'
    })
    res.write(resBody);
    res.end();
  });
}


module.exports = postFiles;
