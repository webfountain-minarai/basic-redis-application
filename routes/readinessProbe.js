const verifyConnection = require('../database/redis').verifyConnection;


async function readinessProbe(req, res) {
  let isReady = false;

  isReady = await verifyConnection();

  const resBody = JSON.stringify({ isReady });
  res.writeHead(200, {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(resBody)
  });
  res.write(resBody);
  res.end();
}


module.exports = readinessProbe;
