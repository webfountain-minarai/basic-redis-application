const url = require('url');
const getFiles = require('./GET_@files');
const postFiles = require('./POST_@files');
const readinessProbe = require('./readinessProbe');
const notFound_404 = require('./notFound_404');


const allowedMethods = ['GET', 'POST'];
const allowedPaths = ['/files', '/readiness'];
const allowedGETpaths = ['/files', '/readiness'];
const allowedPOSTpaths = ['/files'];

function routes() {
  return (req, res) => {
    const { method } = req;
    const { pathname, query } = url.parse(req.url, true);

    console.group('Route Handler');
    console.info({ method, pathname, query: { ...query }});
    console.groupEnd();

    if (!allowedMethods.includes(method) || !allowedPaths.includes(pathname) ||
        (method === 'GET' && !allowedGETpaths.includes(pathname)) ||
        (method === 'POST' && !allowedPOSTpaths.includes(pathname))
    ) notFound_404(req, res);

    if (method === 'GET' && pathname === '/files' && query.name) getFiles(res, query);

    if (method === 'POST' && pathname === '/files') postFiles(req, res);

    if (method === 'GET' && pathname === '/readiness') readinessProbe(req, res);
  };
}


module.exports = routes;
