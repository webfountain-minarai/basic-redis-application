const { promisify } = require('util');
const redis = require('redis');

let reClient;
let getAsync;
let setAsync;
let pingAsync;

const redisConfig = {
  retry_strategy,
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  password: process.env.REDIS_PASSWORD,
  enableOfflineQueue: process.env.REDIS_ENABLEOFFLINEQUEUE,
  prefix: process.env.REDIS_NAMESPACE_PREFIX,
  noReadyCheck: process.env.REDIS_NO_READY_CHECK,
  // retryConnection is not part of the node-redis configuration & is ignored
  // retryConnection is appended here to be used as part of the redis config retry strategy
  retryConnection: {
    periodSeconds: Number(process.env.REDIS_PERIOD_SECONDS),
    timeoutThreshold: Number(process.env.REDIS_TIMEOUT_THRESHOLD)
  }
};

function retry_strategy({ attempt, error, timesConnected, totalRetryTime }) {
  const { periodSeconds, timeoutThreshold } = redisConfig.retryConnection;

  console.error(`Unable to connect to Redis--attempt:${attempt} totalRetryTime:${totalRetryTime/1000}sec timesConnected:${timesConnected}`, { error });

  if (totalRetryTime > timeoutThreshold) {
    console.error(`Retry exhausted--totalRetyTime:${totalRetryTime/1000}sec timeoutThreshold:${timeoutThreshold/1000}sec`)
    return new Error('Retry time exhausted');
  }

  return periodSeconds;
}

async function pingRedis() {
  let ping = '';

  try {
    ping = await pingAsync();
  } catch (err) {
    console.error(`Unable to PING redis -- connectionId::${reClient.connection_id}--${err}`);
    return false;
  }

  console.info(`Redis Connection verified -- connectionId::${reClient.connection_id}--${ping}`);
  return true;
}

function verifyConnection() {
  return async () => {
    console.info(`Verify Connection To Redis -- connectionId::${reClient.connection_id}--PING`);
    return await pingRedis();
  }
}

const _registerEventListeners = () => {
  reClient.on('ready', () => {
    console.info(`Redis client successfully established a connection @${redisConfig.host}:${redisConfig.port}`);
  });

  reClient.on('error', err => {
    console.error(`Redis client --`, { err });
  });

  reClient.on('reconnecting', ({ attempt, delay }) => {
    console.warn(`Redis client@${reClient.connection_id} - attempt:${attempt} - retry every ${delay/1000}sec`);
  });

  reClient.on('end', () => {
    console.warn(`Redis client connection closed @${reClient.connection_id}`);
  });
};

const _configureClient = () => {
  reClient = redis.createClient(redisConfig);

  getAsync = promisify(reClient.get).bind(reClient);
  setAsync = promisify(reClient.set).bind(reClient);
  pingAsync = promisify(reClient.ping).bind(reClient);

  console.info(`Redis client configuration -- connectionId:${reClient.connection_id}`);

  _registerEventListeners();
};

_configureClient();


module.exports = {
  reClient,
  getAsync,
  setAsync,
  verifyConnection: verifyConnection()
};
