A simple NodeJS service that Reads & Writes to Redis.

> `GET /files`  
`POST /files`  
`GET /readiness`  

Used in Part 5 of an introductory blog series `Building a Web Service with Minikube` (in-progress)

#### To run locally (must have redis installed and running -- see resources below)

Let's seed redis with some mock data:

```bash
$ REDISCLI_AUTH=kubernetesapprentice redis-cli
$ 127.0.0.1:6379> SET ba-config_1.json '{"name":"config_1.json","method":"PUT","route":"/a-put-route","headers":{"x-verion":"1.3.5"},"path":["a-put-route"],"query":{}}'
$ 127.0.0.1:6379> SET ba-config_2.json '{"name":"config_2.json","method":"post","route":"/a-post-route","headers":{"x-verion":"0.0.9"},"path":["a-post-route"],"query":{}}'
```

Exit the redis server and start the NodeJS server:

`npm start`  
Navigate to `localhost:3000/files?name=config_1.json`

#### To run w/ Docker

> `<path>` --- add your path to the volume mounts below

Run a Redis container:
```bash
docker run -d --name redis \
--sysctl net.core.somaxconn=511 \
--network two-containers \
--env REDISCLI_AUTH=kubernetesapprentice \
-v /Users/<path>/basic-redis-application/data:/data \
-v /Users/<path>/basic-redis-application/redis.conf:/usr/local/etc/redis/redis.conf \
-p 6379:6379 \
redis:6.0.4-alpine redis-server /usr/local/etc/redis/redis.conf
```

Build and run the NodeJS redis service:  
```bash
$ docker build -t basic-redis-application .
$ docker run -d --name reService \
--network two-containers \
--env-file .env \
-p 3000:3000 \
basic-redis-application
```

#### To run w/ Kubernetes

> `<path>` --- add your path to the volume mount in the `redis-service.yaml`

`cd` into the `_kubeManifests` directory & apply the current contexts

`$ kk apply -f redis-config.yaml`  
`$ kk apply -f redis-service.yaml`  
`$ kk apply -f basic-redis-application.yaml`

#### Resources
* Install redis: https://gist.github.com/tomysmile/1b8a321e7c58499ef9f9441b2faa0aa8
* https://hub.docker.com/_/redis
* https://github.com/NodeRedis/node-redis
* redis issue 55: https://github.com/docker-library/redis/issues/55
* redis issue 35: https://github.com/docker-library/redis/issues/35
* https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
* https://github.com/BretFisher/node-docker-good-defaults/blob/69c923bc646bc96003e9ada55d1ec5ca943a1b19/bin/www
* https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
